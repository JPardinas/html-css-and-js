var database = [
    {
        username: "juan",
        password: "1234"
    },
    {
        username: "pepe",
        password: "2345"
    }
];

var newsFeed = [
    {
        username: "pepe",
        timeline: "Hola soy pepe"
    },
    {
        username: "jose",
        timeline: "Javascript "
    }
];

var userNamePrompt = prompt("What's your username?");
var passWordPrompt = prompt("What's your password?");

function isUserValid (username, password) {

    for (var i = 0; i < database.length; i++) {
        if (database[i].username === username &&
            database[i].password === password) {
                return true;
            } 
    }

    return false;

}

function signIn(user, password) {
    if (isUserValid(user, password)) {
        console.log(newsFeed);
    } else {
        alert("Incorrect username and password");
    }
}

signIn(userNamePrompt, passWordPrompt);