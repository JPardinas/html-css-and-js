
/* TEST FUNCIONES */


function sayHello() {
    console.log("Hello");
}

sayHello();


var sayBye = function() {
    console.log("Bye");
}

sayBye();

function sing(song) {
    console.log(song);
}

sing("LALALLALAA");


function multiply(a, b) {
    return a * b;
}

console.log(multiply(5,10));

/* TEST ARRAYS */
var list = ["tiger", "cat", "bear"];
console.log(list[0])



var functionList = [function apple() {
    console.log("Apple");
}];

functionList[0]();


var testList = ["cat", "bear", "elephant"];

testList = testList.concat(["bee", "deer"]);

console.log(testList);


/* TEST OBJETOS */

var user = {
    name: "John",
    age: 34,
    hobby: "Soccer",
    isMarried: false,
    spells: ["abrakadabra", "shazam"],
    shout: function() {
        console.log("AHHHH!");
    }
};

var userList = [
    {
        username: "andy",
        password: "secre"
    },
    {
        username: "jess",
        password: "1234"
    }
]