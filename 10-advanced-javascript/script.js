//let & const

const playername = 'bobby';
let experiencetest = 100;
let wizardLeveltest = false;


if (experiencetest > 90) {
    let wizardLeveltest = true;
    console.log('inside', wizardLeveltest);
}

console.log('outisde', wizardLeveltest);


const obj = {
    player: 'bobby',
    experience: 100,
    wizardLevel: false
}

const { player, experience } = obj;
console.log(player);
console.log(experience);




const name = 'john snow';

const object = {
    [name]: 'hello',
    ['ray' + 'smith']: 'hihi'
}

console.log(object);


const a = "simon";
const b = true;
const c = {};


const testabc = {
    a: a,
    b: b,
    c: c
}

const testabc2 = {a,b,c}




// Template string

const nametm = "Sally";
const age = 20;
const pet = "horse";

const greeting = `Hello ${nametm} you seem to be ${age}`;
console.log(greeting);

// default arguments

function greet(name='', age=30, pet='cat'){
    return `Hello ${name} you seem to be ${age}`;
};

console.log(greet());
console.log(greet(nametm, age, pet));



// symbol
let sym1 = Symbol();
let sym2 = Symbol('foo');



// arrow functions

function add(a,b) {
    return a + b;
}

const plus = (a, b) => a + b;
const plus2 = (a, b) => {return a + b};




